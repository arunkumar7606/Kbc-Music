package com.arun.aashu.kbc;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    RadioButton b,b4,b2,b3;
    Intent intent;
    AlertDialog.Builder ab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        b= (RadioButton) findViewById(R.id.radioButton);
        b2= (RadioButton) findViewById(R.id.radioButton2);
        b3= (RadioButton) findViewById(R.id.radioButton3);
        b4= (RadioButton) findViewById(R.id.radioButton4);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(b.isChecked()) {
                    ab = new AlertDialog.Builder(Main2Activity.this);
                    ab.setTitle("Sorry You Lose");
                    ab.setMessage("Try Better Luck Next Time");
                    ab.setIcon(R.mipmap.ic_launcher_round);
                    ab.show();

                }

            }
        });


        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(b2.isChecked())
                {
                    ab=new AlertDialog.Builder(Main2Activity.this);
                    ab.setTitle("Sorry You Lose");
                    ab.setMessage("Try Better Luck Next Time");
                    ab.setIcon(R.mipmap.ic_launcher_round);
                    ab.show();

                }

            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(b3.isChecked())
                {
                    ab=new AlertDialog.Builder(Main2Activity.this);
                    ab.setTitle("congradulation you win");
                    ab.setMessage("Now Collect Your 1 crore and go to home");
                    ab.setIcon(R.mipmap.ic_launcher_round);
                    ab.show();

                }
            }
        });

        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (b4.isChecked())
                {
                    ab=new AlertDialog.Builder(Main2Activity.this);
                    ab.setTitle("Sorry You Lose");
                    ab.setMessage("Try Better Luck Next Time");
                    ab.setIcon(R.mipmap.ic_launcher_round);
                    ab.show();
                }

            }
        });



    }
}
