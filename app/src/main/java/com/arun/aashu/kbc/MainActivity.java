package com.arun.aashu.kbc;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    Button b;
    MediaPlayer mp;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b= (Button) findViewById(R.id.button);
        imageView= (ImageView) findViewById(R.id.imageView);
        //imageView.setImageResource(R.drawable.kbcimg);
        mp=MediaPlayer.create(getApplicationContext(),R.raw.waqs);
        mp.start();
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(MainActivity.this,Main2Activity.class);
                mp.stop();
                startActivity(intent

                );
            }
        });

    }
}
